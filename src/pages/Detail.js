import '../App.css';
import {
  useParams
} from "react-router-dom"
import About from './About';

function Detail() {
  let { id } = useParams();

  return (
    <div className="App">
      <header className="App-header">
          <h1>Nama Bahasa: {id}</h1>
          <About
            id={id}
            name={'Ex Ruby'}
          />
      </header>
    </div>
  );
}
export default Detail;
